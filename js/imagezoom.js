/**
 * @file
 * Contains imagezoom.js.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * Initialize image zoom functionality.
   */
  Drupal.behaviors.imagezoom = {
    attach: function (context, drupalSettings) {
      $(once('imagezoom', '.imagezoom-image', context)).each(function () {
        $(this).ezPlus(drupalSettings.imagezoom);
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
