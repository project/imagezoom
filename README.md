# Image Zoom

This module provides a field formatter that allows users to specify an image 
style to display, and another image style to use as the zoomed version of the 
image. Hovering the mouse over the image will display the zoomed version, 
which shifts with mouse movement.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/imagezoom).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/imagezoom).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

To configure the Image Zoom display, go to Administration > Structure > Content
types and select the content type you would want to use Image Zoom with. If you
do not already have an Image field defined, add one by going to the Manage Fields
tab. After you have an Image field, go to the Manage Display tab. Change the
Format for your Image field to Image Zoom. To change which styles are displayed
for the displayed image and the zoomed image, click the gear icon at the end of
the row, select the desired image styles, and click Update.


## Maintainers

- davisben - [davisben](https://www.drupal.org/u/davisben)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
